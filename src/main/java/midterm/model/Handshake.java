package midterm.model;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import java.util.Collection;



@Path("/handshake")
@ApplicationScoped
public class Handshake {


    @GET
    @Produces("text/plain")
    public Response handshake() {
        return Response.ok("handshake").build();
    }
}